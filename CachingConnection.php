<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 11/11/2016
 * Time: 10:59 AM
 */

namespace MindGeek\LDAP;


class CachingConnection
{
    /** @var Connection */
    private $connection;
    /**
     * @var array
     */
    private $cache = [];

    /**
     * @param Connection $connection
     */
    public function decorate(Connection $connection) {
        $this->connection = $connection;
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args) {
        $key = md5($method . serialize($args));
        if(isset($this->cache[$key])) {
            return $this->cache[$key];
        }
        $return = call_user_func_array(array($this->connection, $method), $args);

        $this->cache[md5($method . serialize($args))] = $return;
        return $return;
    }
}