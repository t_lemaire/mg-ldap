<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 11/9/2016
 * Time: 12:22 PM
 */

namespace MindGeek\LDAP;


/**
 * Class Group
 * @package LDAP
 */
class Group extends LDAPObject
{

    /**
     * @var array
     */
    private $members = [];
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $mailnickname = '';

    /**
     * User constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param Connection $connection
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @return array
     */
    public function fetchMembers() {
        return $this->convertToObjects($this->members);
    }


    /**
     * @param array $members
     */
    public function setMembers($members)
    {
        $this->members = $members;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMailNickname()
    {
        return $this->mailnickname;
    }

    /**
     * @param string $mailnickname
     */
    public function setMailNickname($mailnickname)
    {
        $this->mailnickname = $mailnickname;
    }

}