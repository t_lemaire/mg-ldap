<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 2/22/2018
 * Time: 9:48 AM
 */

namespace MindGeek\LDAP;


class LDAPRedisBridge implements BridgeInterface
{
    /** @var LDAPBridge */
    private $ldapBride;
    /** @var \GorillaHub\DataConnectionBundle\Redis|\Redis $redis */
    private $redis;


    /**
     * LDAPRedisBridge constructor.
     * @param \GorillaHub\DataConnectionBundle\Redis|\Redis $redis
     * @param LDAPBridge $ldapBridge
     */
    public function __construct($redis, LDAPBridge $ldapBridge)
    {
        $this->redis = $redis;
        $this->ldapBride = $ldapBridge;
    }

    public function connect()
    {
        $this->ldapBride->connect();
    }

    /**
     * @param string $userName
     * @param string $passWord
     * @return bool
     */
    public function authenticate($userName = null, $passWord = null)
    {
        return $this->ldapBride->authenticate($userName, $passWord);
    }

    /**
     * @param string $base_dn
     * @param string $filter
     * @param array $attributes
     * @return array
     */
    public function search($base_dn, $filter, array $attributes = null)
    {
        $key = 'ldap:' . md5($base_dn. $filter . print_r($attributes, true) );
        $return = '';

        $ret = $this->redis->get($key);
        $expiredValue = true;
        if($ret) {
            $cachedValue = unserialize($ret);
            $return = $cachedValue['data'];
            if ($cachedValue['expire'] > time()) {
                $expiredValue = false;
            }
        }

        if ($expiredValue) {
            $return = $this->ldapBride->search($base_dn, $filter, $attributes);

            $redisData = [
                'data'   => $return,
                'expire' =>  time() + (3600 * 24)
            ];

            $this->redis->set($key, serialize($redisData), 999999);
        }
        return $return;
    }

}