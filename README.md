# LDAP Bundle

### Bundle that include classes for authenticating & querying users and groups via LDAP
	
	
##### Create an LDAP connection instance to query LDAP
```
$ldapConnection = new \MindGeek\LDAP\Connection( $uri, $port, $base, $bind_user, $bind_pass );  
```  
	
	
##### Decorate $ldapConnection with redis caching to speed up queries
```
$ldapRedisCaching = new \MindGeek\LDAP\RedisCachingConnection();  
```  
	
	
##### You can use either a \GorillaHub\DataConnectionBundle\Redis or a native php redis object
Using \GorillaHub\DataConnectionBundle\Redis as an example
```
$redis = new \GorillaHub\DataConnectionBundle\Redis($passWord, $database, $host, $port, $persistent, $connectionType);
$ldapRedisCaching->decorate($ldapConnection, $redis);  
```  
	
	
##### Before running a query on ldap, you will need to first be authenticated, for most cases you can use the default credentials
```
$ldapRedisCaching->authenticate();  
```  
	
	
##### Using LDAP to authenticate credentials
```
$ldapRedisCaching->authenticate(($userName, $passWord));  
```  
	
	
##### Get object by distinguised name
```
$objects = $ldapRedisCaching->getObjectFromDistinguishedNames('CN=Eric Pickup,OU=Users,OU=Engineering,OU=Users-Computers,OU=MTL,OU=LOCATIONS,DC=mgcorp,DC=co');  
```  
	
	
##### Search for matching users
Returns a MindGeek\LDAP\User[] by partial user name $partialName, supports Mozilla LDAP SDK filters
https://wiki.mozilla.org/Mozilla_LDAP_SDK_Programmer%27s_Guide/Searching_the_Directory_With_LDAP_C_SDK 
```
$users = $ldapRedisCaching->searchUsers('*'. $partialName . '*');  
```  
	
	
##### Search for matching groups
Returns a MindGeek\LDAP\User[] by partial group name $partialName, supports Mozilla LDAP SDK filters
https://wiki.mozilla.org/Mozilla_LDAP_SDK_Programmer%27s_Guide/Searching_the_Directory_With_LDAP_C_SDK 
```
$users = $ldapRedisCaching->searchGroups('*'. $partialName . '*');  
```  
	
	
##### Retrieve all email addresses that are a member of a group
$recursivedepth is strongly suggested to not be greater than 5
Returns string[]
```
$emailAddresses = $ldapRedisCaching->getEmailsFromDistinguishedName('CN=Eric Pickup,OU=Users,OU=Engineering,OU=Users-Computers,OU=MTL,OU=LOCATIONS,DC=mgcorp,DC=co', 1);
or
$emailAddresses = $ldapRedisCaching->getEmailsFromDistinguishedName('CN=MTL-Eng_Tubes_Automation,OU=Security-Groups,OU=MTL,OU=LOCATIONS,DC=mgcorp,DC=co', $recursivedepth);  
```  
	
	
