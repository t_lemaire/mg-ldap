<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 11/9/2016
 * Time: 2:38 PM
 */

namespace MindGeek\LDAP;


abstract class LDAPObject
{
    /**
     * @var Connection
     */
    protected $connection = null;

    /**
     * @var string
     */
    protected $samaccountname = '';
    /**
     * @var string
     */
    protected $displayname = '';
    /**
     * @var string
     */
    protected $distinguishedname = '';
    /**
     * @var array
     */
    protected $memberof = [];
    /**
     * @var string
     */
    protected $mail = '';
    /**
     * @var string
     */
    protected $managedby = '';

    /**
     * @var string
     */
    protected $whenCreated;

    /**
     * @return string
     */
    public function getSamAccountName()
    {
        return $this->samaccountname;
    }

    /**
     * @param string $samaccountname
     */
    public function setSamAccountName($samaccountname)
    {
        $this->samaccountname = $samaccountname;
    }

    /**
     * @return string
     */
    public function getDisplayname()
    {
        return $this->displayname;
    }

    /**
     * @param string $displayname
     */
    public function setDisplayname($displayname)
    {
        $this->displayname = $displayname;
    }

    /**
     * @return string
     */
    public function getDistinguishedname()
    {
        return $this->distinguishedname;
    }

    /**
     * @param string $distinguishedname
     */
    public function setDistinguishedname($distinguishedname)
    {
        $this->distinguishedname = $distinguishedname;
    }

    /**
     * @return Group[]
     */
    public function fetchMemberof() {
        return $this->convertToObjects($this->memberof);
    }

    /**
     * @return array
     */
    public function getMemberof()
    {
        return $this->memberof;
    }

    /**
     * @param array $memberof
     */
    public function setMemberof($memberof)
    {
        $this->memberof = $memberof;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getManagedBy()
    {
        return $this->managedby;
    }

    /**
     * @return string
     */
    public function getWhenCreated()
    {
        return $this->whenCreated;
    }

    /**
     * @param string $whenCreated
     */
    public function setWhenCreated($whenCreated)
    {
        $this->whenCreated = $whenCreated;
    }



    /**
     * @return User
     */
    public function fetchManagedBy() {
        $a = $this->convertToObjects($this->managedby);
        return $a[0];
    }

    /**
     * @return Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param Connection $connection
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $managedby
     */
    public function setManagedBy($managedby)
    {
        $this->managedby = $managedby;
    }
    /**
     * @param array $data
     * @return array
     */
    protected function convertToObjects($data) {
        $members = [];
        if(is_array($data)) {
            foreach($data as $member) {
                $parts = explode(',', $member);
                foreach($parts as $part) {
                    $pair = explode('=', $part);
                    $key = $pair[0];
                    $value = $pair[1];
                    if ($key == 'OU' && ($value == 'Security-Groups' || $value == 'Distribution-Groups' || $value == 'Users' || $value == 'Consultants')) {
                        $members[] = $this->connection->getObjectFromDistinguishedNames($member)[0];
                        break;
                    }
                }
            }
        }
        else {
            $members = $this->connection->getObjectFromDistinguishedNames($data);
        }

        return $members;
    }

}
