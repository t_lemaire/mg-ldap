<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 2/22/2018
 * Time: 8:19 AM
 */

namespace MindGeek\LDAP;


class LDAPBridge implements BridgeInterface
{
    /**
     * @var string
     */
    private $uri = '';
    /**
     * @var int
     */
    private $port = 3268;
    /**
     * @var resource
     */
    private $connection = null;

    /**
     * LDAP constructor.
     * @param string $uri
     * @param int $port
     */
    function __construct($uri, $port)
    {
        $this->uri = $uri;
        $this->port = $port;
    }

    /**
     * @throws \Exception
     */
    public function connect() {
        if($this->connection) { return; }

        if (! $this->connection = \ldap_connect($this->uri, $this->port)) {
            throw new \Exception("Unable to connect to LDAP");
        }
        ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->connection, LDAP_OPT_REFERRALS, 0);

        $this->authenticate();
    }

    /**
     * @param string $userName
     * @param string $passWord
     * @return bool
     */
    public function authenticate($userName = null, $passWord = null) {
        $this->connect();
        $old = error_reporting();
        error_reporting(0);
        if (\ldap_bind($this->connection, $userName, $passWord)) {
            error_reporting($old);
            return true;
        }

        error_reporting($old);
    }

    /**
     * @param string $base_dn
     * @param string $filter
     * @param array $attributes
     * @return array
     */
    public function search($base_dn, $filter, array $attributes = null) {
        $this->connect();

        $result = ldap_search($this->connection,
            $base_dn,
            $filter,
            $attributes);
        return ldap_get_entries($this->connection, $result);
    }
}