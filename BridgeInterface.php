<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 2/22/2018
 * Time: 8:21 AM
 */

namespace MindGeek\LDAP;


interface BridgeInterface
{
    public function connect();
    public function authenticate($userName = null, $passWord = null);
    public function search($base_dn, $filter, array $attributes = null);
}