<?php
/**
 * Created by PhpStorm.
 * User: a_kolokathis
 * Date: 2017-09-12
 * Time: 2:56 PM
 */

namespace  MindGeek\LDAP;


class RedisCachingConnection
{
    /** @var Connection */
    private $connection;

    /** @var \GorillaHub\DataConnectionBundle\Redis|\Redis $redis */
    private $redis;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @param Connection $connection
     */
    public function decorate(Connection $connection, $redis) {
        $this->connection = $connection;
        $this->redis = $redis;
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args) {
        $key = 'ldap:' . md5($method . serialize($args));

        $this->connection->authenticate();

        if ($method=="authenticate" && count($args)==0)
        {
            if(isset($this->cache[$key])) {
                return $this->cache[$key];
            }
            $return = call_user_func_array(array($this->connection, $method), $args);

            $this->cache[$key] = $return;
            return $return;
        }

        $ret = $this->redis->get($key);
        $expiredValue = true;
        if($ret) {
            $cachedValue = unserialize($ret);
            $return = $cachedValue['data'];
            if ($cachedValue['expire'] > time()) {
                $expiredValue = false;
            }
        }

        if ($expiredValue) {
            // if the authenticate key did not expire then authentication would have been with cache only
            // we need to do a legit authentication here so that potential ldap call won't fail
            $this->connection->authenticate();
            $return = call_user_func_array(array($this->connection, $method), $args);

            $redisData = [
                'data'   => $return,
                'expire' =>  time() + (3600 * 24)
            ];

            $this->redis->set($key, serialize($redisData), 999999);
        }

        return $return;
    }

}
