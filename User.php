<?php
/**
 * Created by PhpStorm.
 * User: e_pickup
 * Date: 9/13/2016
 * Time: 2:53 PM
 */

namespace MindGeek\LDAP;


class User extends LDAPObject
{
    /**
     * @var string
     */
    private $title = '';
    /**
     * @var string
     */
    private $description = '';
    /**
     * @var string
     */
    private $department = '';
    /**
     * @var string
     */
    private $company = '';
    /**
     * @var string[]
     */
    protected $directReports;
    /**
     * @var string
     */
    protected $employeeType;
    /**
     * @var string
     */
    protected $lastLogonTimestamp;
    /**
     * @var string
     */
    protected $thumbnailPhoto;
    /**
     * User constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }



    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string[]
     */
    public function getDirectReports()
    {
        return $this->directReports;
    }

    /**
     * @param string[] $directReports
     */
    public function setDirectReports($directReports)
    {
        $this->directReports = $directReports;
    }
    /**
     * @return User[]
     */
    public function fetchDirectReports() {
        return $this->convertToObjects($this->directReports);
    }

    /**
     * @return string
     */
    public function getEmployeeType()
    {
        return $this->employeeType;
    }

    /**
     * @param string $employeeType
     */
    public function setEmployeeType($employeeType)
    {
        $this->employeeType = $employeeType;
    }

    /**
     * @return string
     */
    public function getLastLogonTimestamp()
    {
        return $this->lastLogonTimestamp;
    }

    /**
     * @param string $lastLogonTimestamp
     */
    public function setLastLogonTimestamp($lastLogonTimestamp)
    {
        $this->lastLogonTimestamp = $lastLogonTimestamp;
    }

    /**
     * @return string
     */
    public function getThumbnailPhoto()
    {
        return $this->thumbnailPhoto;
    }

    /**
     * @param string $thumbnailPhoto
     */
    public function setThumbnailPhoto($thumbnailPhoto)
    {
        $this->thumbnailPhoto = $thumbnailPhoto;
    }

    /**
     * @return bool
     */
    public function isConsultant() {
        $parts = explode(',', $this->getDistinguishedname());
        foreach($parts as $part) {
            $pair = explode('=', $part);
            $key = $pair[0];
            $value = $pair[1];
            if ($key == 'OU' && ($value == 'Consultants')) {
                return true;
            }
        }
        return false;
    }
}