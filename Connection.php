<?php
namespace MindGeek\LDAP;

use Symfony\Component\Validator\Constraints\DateTime;

class Connection
{
    /**
     * @var BridgeInterface
     */
    private $ldapBridge = '';

    /**
     * @var string
     */
    private $base = '';
    /**
     * @var string
     */
    private $lookupUser = '';
    /**
     * @var string
     */


    /**
     * LDAP constructor.
     * @param BridgeInterface $ldapBridge
     * @param string $base
     * @param string $lookupUser
     * @param string $lookupPass
     */
    function __construct($ldapBridge, $base, $lookupUser, $lookupPass)
    {
        $this->ldapBridge = $ldapBridge;
        $this->base = $base;
        $this->lookupUser = $lookupUser;
        $this->lookupPass = $lookupPass;
    }

    public function connect() {
        $this->ldapBridge->connect();
    }

    /**
     * @param string $userName
     * @param string $passWord
     * @return bool
     */
    public function authenticate($userName = null, $passWord = null) {
        $userName = $userName ? $userName : $this->lookupUser;
        $passWord = $passWord ? $passWord : $this->lookupPass;
        return $this->ldapBridge->authenticate($userName, $passWord);
    }

    /**
     * @param string $text
     * @return User[]
     */
    public function searchUsers($text) {
        $filter = "(&(objectCategory=user)(objectClass=*)(!(objectClass=Phone))(|(displayname=$text)(mail=$text)(samaccountname=$text)))";

        $users = $this->runUserQuery($filter);

        return $users;
    }

    /**
     * @param string $text
     * @return Group[]
     */
    public function searchGroups($text) {
        $filter = "(&(objectCategory=group)(objectClass=*)(|(displayname=$text)(samaccountname=$text)(name=$text)(mail=$text)(mailnickname=$text)))";

        $users = $this->runGroupQuery($filter);

        return $users;
    }

    /**
     * @param string $userName
     * @return User[]
     */
    public function getUsersFromUserName($userName) {
        $userName = ldap_escape($userName);
        $filter = "(&(objectCategory=user)(objectClass=*)(samaccountname=$userName))";

        $users = $this->runUserQuery($filter);

        return $users;
    }

    /**
     * @param string $email
     * @return User[]
     */
    public function getUsersFromEmails($email) {
        $email = ldap_escape($email);
        $filter = "(&(objectCategory=user)(objectClass=*)(mail=$email))";

        $users = $this->runUserQuery($filter);

        return $users;
    }

    /**
     * @param string $name
     * @return User[]
     */
    public function getUsersFromNames($name) {
        $name = ldap_escape($name);
        $filter = "(&(objectCategory=user)(objectClass=*)(givenname=$name))";

        $users = $this->runUserQuery($filter);

        return $users;
    }

    /**
     * @param $name
     * @return User[]|Group[]
     * @throws \Exception
     */
    public function getObjectFromDistinguishedNames($name) {
        $name = ldap_escape($name);

        $filter = "(&(objectCategory=*)(objectClass=*)(distinguishedname=$name))";

        $entries = $this->ldapBridge->search($this->base,
            $filter,
            ["*"]);
        $objects = [];
        foreach($entries as $entry) {
            if (is_array($entry)) {
                $object = null;
                foreach ($entry['objectclass'] as $class) {
                    if ($class == 'organizationalPerson') {
                        $object = $this->convertToUserObject($entry);
                    } elseif ($class == 'group') {
                        $object = $this->convertToGroupObject($entry);
                    }
                }
                $objects[] = $object;
            }
        }
        return $objects;
    }

    /**
     * @param string $name
     * @param int $limit
     * @return string[]
     */
    public function getEmailsFromDistinguishedName($name, $limit = 0)
    {
        $emails = [];
        if($limit == 0){
            return $emails;
        }

        $objects = $this->getObjectFromDistinguishedNames($name);

        foreach($objects as $object)
        {
            if (get_class($object) == "MindGeek\LDAP\User") {
                $emails[$name] = $name;
            }
            else {
                foreach ($object->getMembers() as $identifier)
                {
                    $emails = array_merge($emails, $this->getEmailsFromDistinguishedName($identifier, $limit - 1));
                }
            }
        }

        return $emails;
    }

    /**
     * @param string $name
     * @return User[]
     */
    public function getUsersFromDistinguishedNames($name) {
        $name = ldap_escape($name);
        $filter = "(&(objectCategory=user)(objectClass=*)(distinguishedname=$name))";

        $users = $this->runUserQuery($filter);

        return $users;
    }

    /**
     * @param string $name
     * @return Group[]
     */
    public function getGroupsFromDistinguishedNames($name) {
        $name = ldap_escape($name);
        $filter = "(&(objectCategory=group)(objectClass=*)(distinguishedname=$name))";

        $users = $this->runGroupQuery($filter);

        return $users;
    }

    /**
     * @param string $name
     * @return Group[]
     */
    public function getGroupsFromAccountName($name) {
        $name = ldap_escape($name);
        $filter = "(&(objectCategory=group)(objectClass=*)(sAMAccountName=$name))";

        $users = $this->runGroupQuery($filter);

        return $users;
    }

    /**
     * @param array $entry
     * @return Group
     */
    private function convertToGroupObject($entry) {
        $group = new Group($this);
        if(isset($entry['displayname'])) {
            $group->setDisplayName($entry['displayname'][0]);
        }
        if(isset($entry['distinguishedname'])) {
            $group->setDistinguishedName($entry['distinguishedname'][0]);
        }
        if(isset($entry['name'])) {
            $group->setName($entry['name'][0]);
        }
        if(isset($entry['samaccountname'])) {
            $group->setSamAccountName($entry['samaccountname'][0]);
        }
        if(isset($entry['mail'])) {
            $group->setMail($entry['mail'][0]);
        }
        if(isset($entry['mailnickname'])) {
            $group->setMailNickname($entry['mailnickname'][0]);
        }
        if(isset($entry['managedby'])) {
            $group->setManagedBy($entry['managedby'][0]);
        }

        if(isset($entry['whencreated'])) {
            $group->setWhenCreated($entry['whencreated'][0]);
        }

        $memberOf = [];
        if(isset($entry['memberof'])) {
            foreach ($entry['memberof'] as $offset => $igroup) {
                if (is_int($offset)) {
                    foreach (explode(',', $igroup) as $pair) {
                        $d = explode('=', $pair);
                        if ($d[0] == 'CN') {
                            $memberOf[] = $igroup;
                        }
                    }
                }
            }
        }
        $group->setMemberOf($memberOf);
        $members = [];
        if(isset($entry['member'])) {
            foreach ($entry['member'] as $offset => $igroup) {
                if (is_int($offset)) {
                    foreach (explode(',', $igroup) as $pair) {
                        $d = explode('=', $pair);
                        if ($d[0] == 'CN') {
                            $members[] = $igroup;
                        }
                    }
                }
            }
        }
        $group->setMembers($members);
        return $group;
    }

    /**
     * @param string $filter
     * @return Group[]
     * @throws \Exception
     */
    public function runGroupQuery($filter)
    {

        $entries = $this->ldapBridge->search($this->base,
            $filter,
            ["*"]);

        $groups = [];
        foreach ($entries as $entry) {
            if (is_array($entry)) {
                $groups[] = $this->convertToGroupObject($entry);
            }
        }
        return $groups;
    }

    /**
     * @param array $entry
     * @return User
     */
    private function convertToUserObject($entry){
        $user = new User($this);
        $user->setSamAccountName($entry['samaccountname'][0]);
        if(isset($entry['title'])) {
            $user->setTitle($entry['title'][0]);
        }
        if(isset($entry['title'])) {
            $user->setDescription($entry['title'][0]);
        }
        if(isset($entry['displayname'])) {
            $user->setDisplayname($entry['displayname'][0]);
        }
        if(isset($entry['distinguishedname'])) {
            $user->setDistinguishedname($entry['distinguishedname'][0]);
        }

        if(isset($entry['whencreated'])) {
            $user->setWhenCreated($entry['whencreated'][0]);
        }
        if(isset($entry['directreports'])) {
            $user->setDirectReports($entry['directreports']);
        }
        if(isset($entry['employeetype'])) {
            $user->setEmployeeType($entry['employeetype'][0]);
        }
        if(isset($entry['lastlogontimestamp'])) {
            $user->setLastLogonTimestamp($entry['lastlogontimestamp'][0]);
        }
        if(isset($entry['thumbnailphoto'])) {
            $user->setThumbnailPhoto($entry['thumbnailphoto'][0]);
        }

        $groups = [];
        if(isset($entry['memberof'])) {
            foreach ($entry['memberof'] as $offset => $group) {
                if (is_int($offset)) {
                    foreach (explode(',', $group) as $pair) {
                        $d = explode('=', $pair);
                        if ($d[0] == 'CN') {
                            $groups[] = $group;
                        }
                    }
                }
            }
        }
        $user->setMemberof($groups);
        if(isset($entry['department'])) {
            $user->setDepartment($entry['department'][0]);
        }
        if(isset($entry['company'])) {
            $user->setCompany($entry['company'][0]);
        }
        if(isset($entry['mail'])) {
            $user->setMail($entry['mail'][0]);
        }
        if(isset($entry['manager'])) {
            $user->setManagedBy($entry['manager'][0]);
        }

        return $user;
    }
    /**
     * @param string $filter
     * @return User[]
     * @throws \Exception
     */
    public function runUserQuery($filter) {
        $entries = $this->ldapBridge->search($this->base,
            $filter,
            ["*"]);

        $users = [];
        if (! $entries) {
            return [];
        }

        foreach ($entries as $entry) {
            if (is_array($entry)) {
                $users[] = $this->convertToUserObject($entry);
            }
        }

        return $users;
    }
}
